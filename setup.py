from setuptools import setup

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='poc',
   version='1.0',
   description='Proposer une solution numérique permettant d’identifier tout implant ou prothèse à partir des informations présentes sur son emballage.',
   license="MIT",
   long_description=long_description,
   author='ALAPETITE Nino, AOUAD Mohamed Jad, ARIFA Mohamed Salim, BERRADA Rania, BIRK Renaud, BRETÉCHÉ Youenn, CHEUCLE Antoine, GOLDET Franz, JEMLI Wassel, LAVENAN Kilian, LEFEVRE Antoine, PARION Arnaud, RABIER Nathan',
   author_email='nino.alapetite@imt-atlantique.net, mohamed-jad.aouad@imt-atlantique.net, mohamed-salim.arifa@imt-atlantique.net, rania.berrada@imt-atlantique.net; renaud.birk@imt-atlantique.net, youenn.breteche@imt-atlantique.net, antoine.cheucle@imt-atlantique.net, franz.goldet@imt-atlantique.net, wassel.jemli@imt-atlantique.net, kilian.lavenan@imt-atlantique.net, antoine.lefevre@imt-atlantique.net, arnaud.parion@imt-atlantique.net, nathan.rabier@imt-atlantique.net', 
   url="https://gitlab.imt-atlantique.fr/PC-AP-INT446-N-2023/Hacking-Health/int-hack-groupe7",
   packages=['poc'],
   install_requires=['paddleocr', 'tkinter']
)