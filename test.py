from poc import *
import os

# Constantes
FONT_DIR = "data/font"
FONT_FILENAME = "RobotoSlab-Medium.ttf"
TEST_IMAGES_DIR = "data/img"
OUTPUT_DIR = "out"

def main():
    if not os.path.isdir(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)

    # 1. Phase de reconnaissance optique de caractères
    ocr = OCR(use_gpu=True)
    groups = ocr.recognize(TEST_IMAGES_DIR + "/biomet.jpg")
    file = open(OUTPUT_DIR + "/biomet_export_step1.txt", "w") 
    file.write(str(groups))
    file.close()

    # 1 bis. Génération des images
    ocr.save_ocr(groups, TEST_IMAGES_DIR + "/biomet.jpg", OUTPUT_DIR + "/biomet_export_step1.jpg", FONT_DIR + "/" + FONT_FILENAME)

    # 2. Identification du dispositif
    identification = Identification(groups)
    device = identification.identify()
    output_file = open(OUTPUT_DIR + "/biomet_export_step2.txt", "w") 
    output_file.write(str(device))
    output_file.close()

    # 2 bis. Génération des images
    ocr.save_ocr(device.useful_groups, TEST_IMAGES_DIR + "/biomet.jpg", OUTPUT_DIR + "/biomet_export_step2.jpg", FONT_DIR + "/" + FONT_FILENAME)

    # 3. Interface graphique de tri
    ui = UI(device, OUTPUT_DIR + "/biomet_export_step3.txt")
    ui.show()
    print(ui.device)

    # Autres exemples
    
    # groups2 = ocr.recognize(TEST_IMAGES_DIR + "/passeo-18.jpg")
    # ocr.save_ocr(groups2, TEST_IMAGES_DIR + "/passeo-18.jpg", OUTPUT_DIR + "/passeo-18_export_step1.jpg", FONT_DIR + "/" + FONT_FILENAME)
    # file2 = open(OUTPUT_DIR + "/passeo-18_export_step1.txt", "w") 
    # file2.write(str(groups2))
    # file2.close()
    # identification2 = Identification(groups2)
    # device2 = identification2.identify()
    # output_file2 = open(OUTPUT_DIR + "/passeo-18_export_step2.txt", "w") 
    # output_file2.write(str(device2))
    # output_file2.close()
    # ocr.save_ocr(device2.useful_groups, TEST_IMAGES_DIR + "/passeo-18.jpg", OUTPUT_DIR + "/passeo-18_export_step2.jpg", FONT_DIR + "/" + FONT_FILENAME)
    # ui2 = UI(device2, OUTPUT_DIR + "/passeo-18_export_step3.txt").show()
    # ui2.show()
    # print(ui2.device)

    # groups3 = ocr.recognize(TEST_IMAGES_DIR + "/stryker.jpg")
    # ocr.save_ocr(groups3, TEST_IMAGES_DIR + "/stryker.jpg", OUTPUT_DIR + "/stryker_export_step1.jpg", FONT_DIR + "/" + FONT_FILENAME)
    # file3 = open(OUTPUT_DIR + "/stryker_export_step1.txt", "w") 
    # file3.write(str(groups3))
    # file3.close()
    # identification3 = Identification(groups3)
    # device3 = identification3.identify()
    # output_file3 = open(OUTPUT_DIR + "/stryker_export_step2.txt", "w") 
    # output_file3.write(str(device3))
    # output_file3.close()
    # ocr.save_ocr(device3.useful_groups, TEST_IMAGES_DIR + "/stryker.jpg", OUTPUT_DIR + "/stryker_export_step2.jpg", FONT_DIR + "/" + FONT_FILENAME)
    # ui3 = UI(device3, OUTPUT_DIR + "/stryker_export_step3.txt").show()
    # ui3.show()
    # print(ui3.device)

if __name__ == '__main__':
    main()
