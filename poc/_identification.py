from ._models import *
from typing import List
import csv, re

class Identification:
    """
    Classe de gestion de l'identification et du regroupement des données.
    :param groups: Une liste de groupes trouvés à l'issue de l'OCR
    :param csv_path: Un chemin vers le fichier CSV contenant les informations
    sur les marques référencées
    :param device: Un dispositif
    """

    def __init__(self, groups: List[Group], csv_path="data/csv", device=Device()) -> None:
        self.groups = groups
        self.csv_path = csv_path
        self.device = device

    def extract_column(self, input_file_path: str, output_file_path: str, column_index: int, drop_duplicates=True, case_insensitive=True) -> None:
        """
        Extrait une colonne d'un fichier séparé par des virgules
        (comma-separated values)
        :param input_file_path: Un chemin vers le fichier à traiter
        :param output_file_path: Le chemin où générer le fichier
        :param column_index: Indice de la colonne (en partant de 0)
        :param drop_duplicates: Enlève les répétitions
        :param case_insensitive: Ne respecte pas la casse
        """
        added_rows = set()

        with open(input_file_path, 'r', encoding='utf-8') as input_csv, open(output_file_path, 'w', encoding='utf-8', newline='') as output_csv:
            reader = csv.reader(input_csv)
            writer = csv.writer(output_csv)

            header = next(reader)
            column_name = header[column_index]
            writer.writerow([column_name])

            for row in reader:
                value = row[column_index].lower() if case_insensitive else row[column_index]
                if drop_duplicates:
                    if value not in added_rows:
                        writer.writerow([value])
                        added_rows.add(value)
                else:
                    writer.writerow([value])
        
    def value_in_column(self, file_path: str, value: str, column_index=0) -> bool:
        """
        Vérifie si une valeur fait partie d'une colonne d'un fichier séparé par
        des virgules (comma-separated values)
        :param file_path: Un chemin vers le fichier à traiter
        :param column_index: Indice de la colonne (en partant de 0)
        :param case_insensitive: Ne respecte pas la casse
        """
        with open(file_path, 'r', encoding='utf-8') as file:
            reader = csv.reader(file)
            for row in reader:
                line = row[column_index]
                values = value.split()
                for value_splitted in values:
                    if line.lower() == value_splitted.lower():
                        return True
        return False
    
    def is_brand_name(self, word: str) -> bool:
        """
        Renvoie `True` si le mot passé en entrée correspond à une marque
        présente dans la base de données des marques connues d'Oxyledger.
        :param word: Un mot à chercher
        """
        return self.value_in_column(self.csv_path + "/device_model_extracted_brand_name.csv", word)
    
    def is_manufacturer_name(self, word: str) -> bool:
        """
        Renvoie `True` si le mot passé en entrée correspond à un fabricant
        présent dans la base de données des fabricants connus d'Oxyledger.
        :param word: Un mot à chercher
        """
        return self.value_in_column(self.csv_path + "/device_model_extracted_manufacturer_name.csv", word)
    
    def is_barcode(self, string: str, prefix="") -> bool:
        """
        Teste si la chaîne donnée en paramètres est un code barre en prenant
        en compte d'éventuelles erreurs liées à la phase d'OCR.
        :param string: Une chaîne de caractères à tester
        :param prefix: Le préfixe à trouver
        """
        # Initialisation
        is_prefix = False
        is_prefix_left_error = False

        # Recherche d'un code barre par son préfixe
        if prefix:
            # Cas classique
            is_prefix = string[0:len(prefix)] == prefix
            # Cas où on reconnait 01) au lieu de (01)
            is_prefix_left_error = string[0: len(prefix) - 1] == prefix[1:]
        # Recherche d'un code barre par le format de son préfixe sur au moins 4
        # caractères
        elif len(string) >= 4:
            # Reconnaît le motif (XY) avec X et Y des chiffres
            prefix_pat = re.compile(r"\(\d{2}\)(.*)")
            # Reconnaît le motif XY) avec X et Y des chiffres
            prefix_pat_left_error = re.compile(r"\d{2}\)(.*)")
            # Cas classique
            is_prefix = prefix_pat.fullmatch(string)
            # Cas où on reconnait XY) au lieu de (XY) avec X et Y des chiffres
            is_prefix_left_error = prefix_pat_left_error.fullmatch(string)
        # La fonction renvoie vraie si l'une ou l'autre des conditions est remplie
        return is_prefix or is_prefix_left_error

    def is_tag(self, string: str, tag: str) -> str:
        """
        Teste si la chaîne donnée en paramètres contient un marqueur et renvoie
        sa valeur ou `None` si le marqueur et/ou la valeur n'a/n'ont été(s)
        trouvé(s).
        :param string: Une chaîne de caractères à tester
        :param tag: Le marqueur à trouver
        """
        words = string.split(" ", 1)
        word_left = words[0]
        # S'il y a au moins deux mots et que le premier contient le marqueur
        if len(words) == 2 and tag.lower() in word_left.lower():
            val = words[1].strip()
            # Si la valeur du marqueur fait aux moins 2 caractères, on peut la
            # considérer comme pertinente.
            if len(val) > 2:
                return val
            else:
                return None
        else:
            return None

    def is_date(self, string: str) -> bool:
        """
        Renvoie `True` si la chaîne de caractères passée est une date au format
        YYYY-MM-DD (où YYYY est l'année, MM le numéro du mois et DD le numéro du
        jour du mois).
        :param string: La chaîne de caractères à tester
        """
        pat = re.compile(r'\d{4}-\d{2}-\d{2}')
        return bool(pat.fullmatch(string))

    def is_useful(self, string: str) -> bool:
        """
        Renvoie `True` si la chaîne de caractères passée n'est ni un numéro de
        lot, ni un code barre et ni une date (auquel cas il s'agit d'une
        information pertinente).
        :param chaine: Une chaîne de caractères à tester
        """
        is_batch_number = self.is_tag(string, "lot")
        return not (is_batch_number or self.is_barcode(string) or self.is_date(string))

    def identify(self) -> Device:
        """
        Prend une liste de groupes et retourne un dispositif contenant les
        données déterminées et les données non superflues à classer par
        l'utilisateur.
        """
        self.device = Device()
        for group in self.groups:
            # Scinde les groupes contenant des chaînes de caractères avec au
            # moins deux espaces
            substrings = group.string.split("  ")
            subgroups = [Group(group.box, substring.strip(), group.score) for substring in substrings]
            for subgroup in subgroups:
                # Vérifie si la chaîne n'est pas vide
                if subgroup.string:
                    # Si la chaîne analysée commence par (01), alors on retire
                    # le préfixe "(01)", on obtient ainsi l'IUD
                    if self.is_barcode(subgroup.string, "(01)"):
                        self.device.uid = subgroup.string[3:].split("(", 1)[0].replace(" ", "").replace(")", "")
                    else:
                        # Si la chaîne analysée est aux abords d'un marqueur
                        # "ref", alors il s'agit d'une référence
                        ref = self.is_tag(subgroup.string, "ref")
                        if ref:
                            self.device.ref = ref
                        # Si on a trouvé la référence du dispositif, on lui
                        # ajoute
                        if self.is_useful(subgroup.string):
                            if len(subgroup.string) > 2:
                                if self.is_brand_name(subgroup.string):
                                    self.device.brand_name = subgroup.string
                                elif self.is_manufacturer_name(subgroup.string):
                                    self.device.manufacturer_name = subgroup.string
                                else:
                                    # On retire les numéros de lots, les autres codes barres
                                    # et les dates qui ne sont pas utiles pour la suite du
                                    # processus
                                    self.device.useful_groups.append(subgroup)
        return self.device
