from ._models import Group
from paddleocr import PaddleOCR, draw_ocr
from PIL import Image
from typing import List

class OCR:
    """
    Classe d'implémentation de la reconnaissance optique de caractères (OCR).
    :param use_gpu: `True` si paddlepaddle-gpu doit être utilisé, `False` si
    paddlepaddle doit être utilisé
    """
    
    def __init__(self, use_gpu=True) -> None:
        # Il n'y a besoin d'exécuter une seule fois pour télécharger et charger
        # le modèle en mémoire
        # Le modèle de langage "en" a été choisi car c'est celui qui donne les
        # meilleurs résultats parmi les modèles "cn", "en" et "fr"
        self.ocr = self.__ocr_initialize__(use_gpu=use_gpu)

    def __ocr_initialize__(self, lang='en', use_gpu=True) -> PaddleOCR:
        """
        Télécharge et charge le modèle d'OCR en mémoire et renvoie l'objet
        correspondant.
        :param lang: Un modèle de langage à utiliser
        :param use_gpu: `True` si paddlepaddle-gpu doit être utilisé, `False` si
        paddlepaddle doit être utilisé
        """
        return PaddleOCR(use_angle_cls=True, lang=lang, use_gpu=use_gpu) 

    def recognize(self, img_path: str) -> List[Group]:
        """
        Méthode de reconnaissance optique de caractères qui renvoie la liste des
        groupes trouvés.
        :param img_path: Un chemin vers l'image à utiliser
        """
        groups = []
        result = self.ocr.ocr(img_path, cls=True)
        res = result[0]
        for line in res:
            grp = Group(line[0], line[1][0], line[1][1])
            groups.append(grp)
        return groups
    
    def save_ocr(self, groups: List[Group], img_path: str, output_img_path: str, font_path: str) -> Image:
        """
        Génère une image d'illustration des résultats et la renvoie.
        :param groups: Une liste de groupes trouvés
        :param img_path: Un chemin vers l'image à analyser
        :param output_img_path: Un chemin vers l'image de sortie
        :param font_path: Un chemin vers une police d'écriture (au format TrueType Font)
        """
        image = Image.open(img_path).convert('RGB')
        boxes = [line.box for line in groups]
        txts = [line.string for line in groups]
        scores = [line.score for line in groups]
        im_show = draw_ocr(image, boxes, txts, scores, font_path=font_path)
        im_show = Image.fromarray(im_show) 
        im_show.save(output_img_path)
        return im_show
