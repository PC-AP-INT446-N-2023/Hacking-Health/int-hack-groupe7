from ._models import *
from tkinter import *

class UI:
    """
    Classe définissant une interface graphique remplie à l'issue de la phase
    d'identification.
    :param device: Un dispositif issu de la phase d'identification
    :param export_path: Un chemin d'exportation
    """

    def __init__(self, device: Device, export_path: str=None) -> None:
        self.device = device
        self.export_path = export_path
    
    def clear(self) -> None:
        """
        Nettoie la fenêtre.
        """
        for widget in self.app.winfo_children():
            widget.grid_forget()
    
    def draw(self) -> None:
        """
        Dessine les composants sur la fenêtre.
        """
        # S'il n'y a plus d'élements à trier, on arrête l'application
        if not len(self.device.useful_groups):
            del self.device.useful_groups
            self.export()
            self.app.destroy()
            return

        word_entry = Entry(self.app, justify='center', width=70)
        word_entry.insert(0, self.device.useful_groups.pop(0).string)
        word_entry.grid(row=1, column=1, columnspan=4)
        
        uid_button = Button(self.app, text="IUD", command=lambda: self.updateField(uid_entry=word_entry))
        uid_button.grid(row=2, column=1)
        
        manufacturer_button = Button(self.app, text="Fabricant", command=lambda: self.updateField(manufacturer_name_entry=word_entry))
        manufacturer_button.grid(row=2, column=2)

        brand_button = Button(self.app, text="Marque", command=lambda: self.updateField(brand_name_entry=word_entry))
        brand_button.grid(row=2, column=3)
        
        ref_button = Button(self.app, text="Reférence", command=lambda: self.updateField(ref_entry=word_entry))
        ref_button.grid(row=2, column=4)
        
        uid_entry = Entry(self.app, justify='center', width=20)
        uid_entry.insert(0, self.device.uid)
        uid_entry.grid(row=3, column=1)
        
        manufacturer_name_entry = Entry(self.app, justify='center', width=20)
        manufacturer_name_entry.insert(0, self.device.manufacturer_name)
        manufacturer_name_entry.grid(row=3, column=2)

        brand_name_entry = Entry(self.app, justify='center', width=20)
        brand_name_entry.insert(0, self.device.brand_name)
        brand_name_entry.grid(row=3, column=3)
        
        ref_entry = Entry(self.app, justify='center', width=20)
        ref_entry.insert(0, self.device.ref)
        ref_entry.grid(row=3, column=4)

        description_button = Button(self.app, text="Description", command = lambda: self.updateField(description=word_entry))
        description_button.grid(row=4, column=2, columnspan=2)
        
        trash_button = Button(self.app, text="Poubelle", command = self.updateField)
        trash_button.grid(row=4, column=1)
        
        exit_button = Button(self.app, text="Quitter", command = self.app.destroy)
        exit_button.grid(row=4, column=4)

    def export(self, destroy=None) -> None:
        """
        Si le chemin d'export du fichier est défini on exporte le fichier au
        format de dictionnaire Python.
        """
        if self.export_path:
            file = open(self.export_path, "w")
            file.write(str(self.device))
            file.close()
    
    def show(self) -> None:
        """
        Initialise et affiche la fenêtre.
        """
        self.app = Tk()
        self.app.eval('tk::PlaceWindow . center')
        self.app.geometry("656x240")
        self.app.grid()
        self.app.columnconfigure(1, minsize=150)
        self.app.columnconfigure(2, minsize=150)
        self.app.columnconfigure(3, minsize=150)
        self.app.columnconfigure(4, minsize=150)
        self.app.rowconfigure(1, minsize=60)
        self.app.rowconfigure(2, minsize=60)
        self.app.rowconfigure(3, minsize=60)
        self.app.rowconfigure(4, minsize=60)
        self.draw()
        self.app.mainloop()
    
    def updateField(self, uid_entry: Entry=None, manufacturer_name_entry: Entry=None, brand_name_entry: Entry=None, ref_entry: Entry=None, description: str=None) -> None:
        """
        Met à jour les champs du dispositif selon les informations remplies
        dans les composants de la fenêtre.
        :param uid_entry: Champ de texte de l'IUD
        :param manufacturer_name_entry: Champ de texte du fabricant
        :param brand_name_entry: Champ de texte de la marque
        :param ref_entry: Champ de texte de la référence
        :param description: Description
        """
        if uid_entry:
            self.device.uid = uid_entry.get()
        if manufacturer_name_entry:
            self.device.manufacturer_name = manufacturer_name_entry.get()
        if brand_name_entry:
            self.device.brand_name = brand_name_entry.get()
        if ref_entry:
            self.device.ref = ref_entry.get()
        if description:
            self.device.description += description.get() + "\n"
        self.clear()
        self.draw()
