"""
Preuve de concept d'une solution numérique permettant d'identifier tout implant
ou prothèse à partir des informations présentes sur son emballage.
"""

__all__ = [
    "Device", "Group", "Identification", "OCR", "UI"
]

from ._identification import Identification
from ._models import Device, Group
from ._ocr import OCR
from ._ui import UI
