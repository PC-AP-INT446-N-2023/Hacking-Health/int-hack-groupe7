from typing import List

class Group:
    """
    Classe représentant un regroupement (boîte, chaîne de caractères, score)
    en aval d'une OCR.
    :param box: La liste des boîtes de caractères. Une boîte est une liste de 4
    flottants représentant les 4 coins du cadre englobant les caractères
    reconnus
    :param string: La chaîne de caractères reconnue
    :param score: La précision de cette reconnaissance
    """

    def __init__(self, box: List[List[float]], string: str, score: float) -> None:
        self.box = box
        self.string = string
        self.score = score
    
    def __repr__(self) -> str:
        return self.__dict__.__str__()
    
class Device:
    """
    Classe représentant les informations d'identification d'un dispositif.
    :param uid: L'identificateur unique (IUD)
    :param manufacturer_name: Le fabricant
    :param brand_name: La marque
    :param description: Une description
    :param ref: La référence
    :param useful_groups: La liste de groupes à trier
    """

    def __init__(self, uid="", manufacturer_name="", brand_name="", description="", ref="", useful_groups: List[Group]=list()) -> None:
        self.uid = uid
        self.manufacturer_name = manufacturer_name
        self.brand_name = brand_name
        self.description = description
        self.ref = ref
        self.useful_groups = useful_groups
    
    def __repr__(self) -> str:
        return self.__dict__.__str__()
