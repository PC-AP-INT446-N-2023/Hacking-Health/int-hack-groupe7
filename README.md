# INT-HACK-Groupe7

Proposer une solution numérique permettant d’identifier tout implant ou prothèse à partir des informations présentes sur son emballage. 

## Installation

### Installation de PaddlePaddle

> Si vous n'avez pas d'environnement Python, reportez-vous au [manuel d'installation de Python](https://docs.python.org/fr/3/using/index.html).

Utiliser le gestionnaire de paquets [pip](https://pip.pypa.io/en/stable/) pour installer PaddlePaddle.

- Si CUDA 9 ou CUDA 10 est installé sur votre machine, veuillez exécuter la commande suivante pour installer

```bash
python -m pip install paddlepaddle-gpu -i https://pypi.tuna.tsinghua.edu.cn/simple
```

- Si vous n'avez pas de GPU disponible sur votre machine, veuillez exécuter la commande suivante pour installer la version du CPU

```bash
python -m pip install paddlepaddle -i https://pypi.tuna.tsinghua.edu.cn/simple
```

- **Note pour les utilisateurs de paddlepaddle :** Utilisez `ocr = OCR(use_gpu=False)` à l'utilisation.

Pour plus d'informations sur les exigences de version du logiciel, veuillez vous reporter aux instructions du [document d'installation](https://www.paddlepaddle.org.cn/en/install/quick?docurl=/documentation/docs/en/install/pip/macos-pip_en.html) pour le fonctionnement.

### Installation de PaddleOCR

```bash
pip install "paddleocr>=2.0.1"
```

## Utilisation

### Utilisation du prototype

```
python test.py
```

Vous pouvez ajouter vos images de tests dans le dossier `data/img/`.

### Utilisation par le code

Depuis la racine du projet :

```python
from poc import *

# Code...
```

## Contribution

Si vous avez toujours voulu contribuer à l'open source et à une grande cause, c'est maintenant votre chance !

## Auteurs et remerciements

### Auteurs

- Nino Alapetite
- Mohamed Jad Aouad
- Mohamed Salim Arifa
- Rania Berrada
- Renaud Birk
- Youenn Bretéché
- Antoine Cheucle
- Franz Goldet
- Wassel Jemli
- Kilian Lavenan
- Antoine Lefevre
- Arnaud Parion
- Nathan Rabier

### Remerciements

- Clément Lesain, Ingénieur R&D
- Xavier Moal, directeur général de l'entreprise Oxyledger

## Licence

[MIT](https://choosealicense.com/licenses/mit/)
